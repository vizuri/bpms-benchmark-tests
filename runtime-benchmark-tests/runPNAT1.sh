#/bin/sh

PARAMS="-Plocal -Djbpm.runtimeManagerStrategy=PerProcessInstance -DreportDataLocation=results/persistance_no_audit/threads_1 -Dsuite=ConcurrentLoadSuite -Dthreads=1 -Diterations=10 -DwarmUp=true -DwarmUpCount=10 -DauditLogging=false -Djbpm.persistence=true -Dscenario=LStartEndProcess"

mvn clean install exec:exec $PARAMS
