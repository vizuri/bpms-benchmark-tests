#/bin/sh

GLOBAL_PARAMS="-Diterations=10  -DreporterType=Console -Djbpm.runtimeManagerStrategy=PerProcessInstance"
REPORT_BASE=results_medium_test
rm -fr $REPORT_BASE

###############################################
#Persistence with Auditing 
###############################################
REPORT_LOCATION=${REPORT_BASE}/persistance_with_auditing
PERSISTENCE=true
AUDITING=true

###############################################
#1 Thread
###############################################
THREADS=1

mvn clean install exec:exec $GLOBAL_PARAMS -Dsuite=ConcurrentLoadSuite -Dthreads=$THREADS -DauditLogging=${AUDITING} -Djbpm.persistence=${PERSISTENCE} -Dscenario=LStartEndProcess

