#/bin/sh

PARAMS="-Djbpm.runtimeManagerStrategy=PerProcessInstance -DreportDataLocation=results/persistance/threads_1 -Dsuite=ConcurrentLoadSuite -Dthreads=1 -Diterations=10 -DwarmUp=true -DwarmUpCount=10 -DauditLogging=false"

#-DreporterType=CSVSingle

mvn clean install exec:exec $PARAMS
