package org.kie.perf.scenario.load;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.api.event.process.DefaultProcessEventListener;
import org.kie.api.event.process.ProcessCompletedEvent;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.task.TaskService;
import org.kie.perf.SharedMetricRegistry;
import org.kie.perf.jbpm.JBPMController;

import org.kie.perf.scenario.IPerfTest;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;

public class LHumanTaskProcess implements IPerfTest {

    private JBPMController jc;

    private Timer startProcess;
    private Timer queryTaskDuration;
    private Timer startTaskDuration;
    private Timer completeTaskDuration;
//    private Timer completeProcessDuration;
	private Meter completedProcess;

    
	@Override
	public void init() {
		jc = JBPMController.getInstance();
		jc.setProcessEventListener(new DefaultProcessEventListener() {
			@Override
			public void afterProcessCompleted(ProcessCompletedEvent event) {
				completedProcess.mark();
			}
		});
		 jc.createRuntimeManager();
	}
	

    @Override
    public void initMetrics() {
        MetricRegistry metrics = SharedMetricRegistry.getInstance();
		completedProcess = metrics
				.meter(MetricRegistry.name(LHumanTaskProcess.class, "scenario.process.completed"));
        startProcess = metrics.timer(MetricRegistry.name(LHumanTaskProcess.class, "scenario.process.start.duration"));
        queryTaskDuration = metrics.timer(MetricRegistry.name(LHumanTaskProcess.class, "scenario.task.query.duration"));
        startTaskDuration = metrics.timer(MetricRegistry.name(LHumanTaskProcess.class, "scenario.task.start.duration"));
        completeTaskDuration = metrics.timer(MetricRegistry.name(LHumanTaskProcess.class, "scenario.task.complete.duration"));
//	    completeProcessDuration = metrics.timer(MetricRegistry.name(LHumanTaskProcess.class, "scenario.process.complete.duration"));
    }

    @Override
    public void execute() {
        Timer.Context context;
//		Timer.Context totalContext;
//		totalContext = completeProcessDuration.time();

        context = startProcess.time();
        RuntimeEngine runtimeEngine = jc.getRuntimeEngine();
        KieSession ksession = runtimeEngine.getKieSession();
        ProcessInstance pi = ksession.startProcess("bpms-benchmark-scenarios.HumanTask");
        context.stop();

        context = queryTaskDuration.time();
        TaskService taskService = runtimeEngine.getTaskService();
        List<Long> tasks = taskService.getTasksByProcessInstanceId(pi.getId());
        Long taskSummaryId = tasks.get(0);
        context.stop();
        
        context = startTaskDuration.time();
        taskService.start(taskSummaryId, "buser");
        context.stop();

        context = completeTaskDuration.time();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("out_approved", true);
        taskService.complete(taskSummaryId, "buser", params);
        context.stop();
//        totalContext.stop();
    }

    @Override
    public void close() {
        jc.tearDown();
    }

}
