package org.kie.perf.scenario.load;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.event.process.DefaultProcessEventListener;
import org.kie.api.event.process.ProcessCompletedEvent;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.perf.SharedMetricRegistry;
import org.kie.perf.jbpm.JBPMController;
import org.kie.perf.scenario.IPerfTest;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;

public class LSignalProcess implements IPerfTest {

	private JBPMController jc;

	private Timer startProcess;
	private Timer signalDuration;
//    private Timer completeProcessDuration;
	private Meter completedProcess;

	@Override
	public void init() {
		jc = JBPMController.getInstance();
		jc.setProcessEventListener(new DefaultProcessEventListener() {
			@Override
			public void afterProcessCompleted(ProcessCompletedEvent event) {
				completedProcess.mark();
			}
		});

		jc.createRuntimeManager();
	}

	@Override
	public void initMetrics() {
		MetricRegistry metrics = SharedMetricRegistry.getInstance();
		completedProcess = metrics
				.meter(MetricRegistry.name(LSignalProcess.class, "scenario.process.completed"));
 		startProcess = metrics
				.timer(MetricRegistry.name(LSignalProcess.class, "scenario.process.start.duration"));
		signalDuration = metrics.timer(MetricRegistry.name(LSignalProcess.class, "scenario.signal.duration"));
//	    completeProcessDuration = metrics.timer(MetricRegistry.name(LExternalSignalProcess.class, "scenario.process.complete.duration"));
	}

	@Override
	public void execute() {
		Timer.Context context;
//		Timer.Context totalContext;
//		totalContext = completeProcessDuration.time();
		context = startProcess.time();
		RuntimeEngine runtimeEngine = jc.getRuntimeEngine();
		KieSession ksession = runtimeEngine.getKieSession();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("counter", 0);
		ProcessInstance pi = ksession.startProcess("bpms-benchmark-scenarios.ExternalSignal", params);
		context.stop();

		context = signalDuration.time();
		ksession.signalEvent("ExternalSignal", "value", pi.getId());
		context.stop();
//		totalContext.stop();

	}

	@Override
	public void close() {
		jc.tearDown();
	}

}
