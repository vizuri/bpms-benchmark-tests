package org.kie.perf.scenario.load;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.event.process.DefaultProcessEventListener;
import org.kie.api.event.process.ProcessCompletedEvent;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.perf.SharedMetricRegistry;
import org.kie.perf.jbpm.JBPMController;
import org.kie.perf.scenario.IPerfTest;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;

public class LSequentialFlowProcess implements IPerfTest {

    private JBPMController jc;

    private Meter completedProcess;
    private Timer completeProcessDuration;

    @Override
    public void init() {
        jc = JBPMController.getInstance();
        jc.setProcessEventListener(new DefaultProcessEventListener() {
            @Override
            public void afterProcessCompleted(ProcessCompletedEvent event) {
                completedProcess.mark();
            }
        });

        jc.createRuntimeManager();
    }

    @Override
    public void initMetrics() {
        MetricRegistry metrics = SharedMetricRegistry.getInstance();
        completedProcess = metrics.meter(MetricRegistry.name(LSequentialFlowProcess.class, "scenario.process.completed"));
        completeProcessDuration = metrics.timer(MetricRegistry.name(LSequentialFlowProcess.class, "scenario.process.complete.duration"));
   }

    @Override
    public void execute() {
        Timer.Context context;

        context = completeProcessDuration.time();
        RuntimeEngine runtimeEngine = jc.getRuntimeEngine();
        KieSession ksession = runtimeEngine.getKieSession();
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("counter", 0);


        ksession.startProcess("bpms-benchmark-scenarios.SequentialFlow",params);
        context.stop();
    }

    @Override
    public void close() {
        jc.tearDown();
    }

}
