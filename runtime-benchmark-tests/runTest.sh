#/bin/sh

GLOBAL_PARAMS="-Diterations=1000  -DreporterType=CSV -Djbpm.runtimeManagerStrategy=PerProcessInstance"
REPORT_BASE=results_test
rm -fr $REPORT_BASE

###############################################
#No Persistence
###############################################
REPORT_LOCATION=${REPORT_BASE}/no_persistance
PERSISTENCE=false
AUDITING=false

###############################################
#1 Thread
###############################################
THREADS=1

for SCENARIO in LStartEndProcess LSequentialFlowProcess LParallelGatewayProcess LRuleTaskProcess LExternalSignalProcess
do
	mvn clean install exec:exec $GLOBAL_PARAMS -DreportDataLocation=${REPORT_LOCATION}/threads_${THREADS} -Dsuite=ConcurrentLoadSuite -Dthreads=$THREADS -DwarmUp=true -DwarmUpCount=10 -DauditLogging=${AUDITING} -Djbpm.persistence=${PERSISTENCE} -Dscenario=${SCENARIO}
done

###############################################
#10 Threads
###############################################
THREADS=10

for SCENARIO in LStartEndProcess LSequentialFlowProcess LParallelGatewayProcess LRuleTaskProcess LExternalSignalProcess
do
	mvn clean install exec:exec $GLOBAL_PARAMS -DreportDataLocation=${REPORT_LOCATION}/threads_${THREADS} -Dsuite=ConcurrentLoadSuite -Dthreads=$THREADS -DwarmUp=true -DwarmUpCount=10 -DauditLogging=${AUDITING} -Djbpm.persistence=${PERSISTENCE} -Dscenario=${SCENARIO}
done

###############################################
#25 Threads
###############################################
THREADS=25

for SCENARIO in LStartEndProcess LSequentialFlowProcess LParallelGatewayProcess LRuleTaskProcess LExternalSignalProcess
do
	mvn clean install exec:exec $GLOBAL_PARAMS -DreportDataLocation=${REPORT_LOCATION}/threads_${THREADS} -Dsuite=ConcurrentLoadSuite -Dthreads=$THREADS -DwarmUp=true -DwarmUpCount=10 -DauditLogging=${AUDITING} -Djbpm.persistence=${PERSISTENCE} -Dscenario=${SCENARIO}
done

###############################################
#50 Threads
###############################################
THREADS=50

for SCENARIO in LStartEndProcess LSequentialFlowProcess LParallelGatewayProcess LRuleTaskProcess LExternalSignalProcess
do
	mvn clean install exec:exec $GLOBAL_PARAMS -DreportDataLocation=${REPORT_LOCATION}/threads_${THREADS} -Dsuite=ConcurrentLoadSuite -Dthreads=$THREADS -DwarmUp=true -DwarmUpCount=10 -DauditLogging=${AUDITING} -Djbpm.persistence=${PERSISTENCE} -Dscenario=${SCENARIO}
done

