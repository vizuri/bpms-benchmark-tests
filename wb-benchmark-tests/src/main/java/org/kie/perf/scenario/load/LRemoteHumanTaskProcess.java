package org.kie.perf.scenario.load;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbpm.process.audit.ProcessInstanceLog;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.task.TaskService;
import org.kie.internal.runtime.manager.context.EmptyContext;
import org.kie.perf.SharedMetricRegistry;
import org.kie.perf.remote.KieWBTestConfig;
import org.kie.perf.remote.RESTClient;
import org.kie.perf.remote.RemoteController;
import org.kie.perf.scenario.IPerfTest;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;

public class LRemoteHumanTaskProcess implements IPerfTest {

    private RemoteController rc;

    private Meter completedProcess;
    private Timer startProcess;
    private Timer queryTaskDuration;
    private Timer startTaskDuration;
    private Timer completeTaskDuration;

    @Override
    public void init() {
        //rc = RemoteControllerProvider.getRemoteController(KieWBTestConfig.DEPLOYMENT_ID);
        rc =  new RESTClient(KieWBTestConfig.DEPLOYMENT_ID);
    }

    @Override
    public void initMetrics() {
        MetricRegistry metrics = SharedMetricRegistry.getInstance();
        completedProcess = metrics.meter(MetricRegistry.name(LRemoteHumanTaskProcess.class, "scenario.process.completed"));
        startProcess = metrics.timer(MetricRegistry.name(LRemoteHumanTaskProcess.class, "scenario.process.start.duration"));
        queryTaskDuration = metrics.timer(MetricRegistry.name(LRemoteHumanTaskProcess.class, "scenario.task.query.duration"));
        startTaskDuration = metrics.timer(MetricRegistry.name(LRemoteHumanTaskProcess.class, "scenario.task.start.duration"));
        completeTaskDuration = metrics.timer(MetricRegistry.name(LRemoteHumanTaskProcess.class, "scenario.task.complete.duration"));
    }

    @Override
    public void execute() {
        Timer.Context context;
        ProcessInstance pi = rc.newPerProcessInstanceController(null).startProcess("bpms-benchmark-scenarios.HumanTask");
        
        RemoteController taskService = rc.newPerProcessInstanceController(pi.getId());
        
        
       // TaskService taskService = runtimeEngine.getTaskService();
        
        context = queryTaskDuration.time();
        List<Long> tasks = taskService.getTasksByProcessInstanceId(pi.getId());
        Long taskSummaryId = tasks.get(0);
        context.stop();
        
        context = startTaskDuration.time();
        taskService.start(taskSummaryId, "buser");
        context.stop();

        context = completeTaskDuration.time();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("out_approved", true);
        taskService.complete(taskSummaryId, "buser", params);
        context.stop();
        
  
        
        org.kie.api.runtime.manager.audit.ProcessInstanceLog plog = rc.getAuditService().findProcessInstance(pi.getId());
        context.stop();

        if (plog != null && plog.getStatus() == ProcessInstance.STATE_COMPLETED) {
            completedProcess.mark();
        }

        
//        pi = taskService.getProcessInstance(pi.getId());
//        if (pi != null && pi.getState() == ProcessInstance.STATE_COMPLETED) {
//            completedProcess.mark();
//        }
    }

    @Override
    public void close() {

    }

}
