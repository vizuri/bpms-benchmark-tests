package org.kie.perf.scenario.load;

import org.kie.api.runtime.process.ProcessInstance;
import org.kie.perf.SharedMetricRegistry;
import org.kie.perf.remote.KieWBTestConfig;
import org.kie.perf.remote.RESTClient;
import org.kie.perf.remote.RemoteController;
import org.kie.perf.scenario.IPerfTest;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;

public class LRemoteStartEndProcess implements IPerfTest {

    private RemoteController rc;

    private Meter completedProcess;

    @Override
    public void init() {
        //rc = RemoteControllerProvider.getRemoteController(KieWBTestConfig.DEPLOYMENT_ID);
        rc =  new RESTClient(KieWBTestConfig.DEPLOYMENT_ID);
    }

    @Override
    public void initMetrics() {
        MetricRegistry metrics = SharedMetricRegistry.getInstance();
        completedProcess = metrics.meter(MetricRegistry.name(LRemoteStartEndProcess.class, "scenario.process.completed"));
    }

    @Override
    public void execute() {
    	
        ProcessInstance pi = rc.newPerProcessInstanceController(null).startProcess("bpms-benchmark-scenarios.StartEnd");
        if (pi != null && pi.getState() == ProcessInstance.STATE_COMPLETED) {
            completedProcess.mark();
        }
    }

    @Override
    public void close() {

    }

}
