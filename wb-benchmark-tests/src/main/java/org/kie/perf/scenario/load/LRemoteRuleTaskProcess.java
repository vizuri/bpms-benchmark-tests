package org.kie.perf.scenario.load;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.ProcessInstance;
import org.kie.perf.SharedMetricRegistry;
import org.kie.perf.remote.KieWBTestConfig;
import org.kie.perf.remote.RESTClient;
import org.kie.perf.remote.RemoteController;
import org.kie.perf.scenario.IPerfTest;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.vizuri.bpms.bpms_benchmark_scenarios.Person;

public class LRemoteRuleTaskProcess implements IPerfTest {

    private RemoteController rc;

    private Meter completedProcess;

    @Override
    public void init() {
        //rc = RemoteControllerProvider.getRemoteController(KieWBTestConfig.DEPLOYMENT_ID);
        rc =  new RESTClient(KieWBTestConfig.DEPLOYMENT_ID);
    }

    @Override
    public void initMetrics() {
        MetricRegistry metrics = SharedMetricRegistry.getInstance();
        completedProcess = metrics.meter(MetricRegistry.name(LRemoteRuleTaskProcess.class, "scenario.process.completed"));
    }

    @Override
    public void execute() {
        Map<String, Object> params = new HashMap<String, Object>();
        Person p = new Person();
        p.setFirstName("Test");
        p.setLastName("Person");
        p.setAge(25);
        params.put("person", p);
        ProcessInstance pi = rc.newPerProcessInstanceController(null).startProcess("bpms-benchmark-scenarios.RulesProcess", params);
        
        if (pi != null && pi.getState() == ProcessInstance.STATE_COMPLETED) {
            completedProcess.mark();
        }
    }

    @Override
    public void close() {

    }

}
