#/bin/sh

GLOBAL_PARAMS="-Diterations=1000  -DreporterType=CSVSingle -Dworkbench.username=bpmsAdmin -Dworkbench.password=P@ssw0rd -Dworkbench.host=172.31.3.11 -Dworkbench.port=8080 -Dworkbench.remotingPort=4447 -Dworkbench.name=business-central"



REPORT_BASE=results-medium-remote
rm -fr $REPORT_BASE

###############################################
#No Persistence
###############################################
REPORT_LOCATION=${REPORT_BASE}

###############################################
#1 Thread
###############################################
THREADS=1

for SCENARIO in LRemoteStartEndProcess LRemoteSequentialFlowProcess LRemoteParallelGatewayProcess LRemoteRuleTaskProcess LRemoteExternalSignalProcess LRemoteHumanTaskProcess
do
	mvn clean install exec:exec $GLOBAL_PARAMS -DreportDataLocation=${REPORT_LOCATION}/threads_${THREADS} -Dsuite=ConcurrentLoadSuite -Dthreads=$THREADS -DwarmUp=true -DwarmUpCount=10 -Dscenario=${SCENARIO}
done

###############################################
#10 Threads
###############################################
THREADS=10

for SCENARIO in LRemoteStartEndProcess LRemoteSequentialFlowProcess LRemoteParallelGatewayProcess LRemoteRuleTaskProcess LRemoteExternalSignalProcess LRemoteHumanTaskProcess
do
	mvn clean install exec:exec $GLOBAL_PARAMS -DreportDataLocation=${REPORT_LOCATION}/threads_${THREADS} -Dsuite=ConcurrentLoadSuite -Dthreads=$THREADS -DwarmUp=true -DwarmUpCount=10 -Dscenario=${SCENARIO}
done

###############################################
#25 Threads
###############################################
THREADS=25

for SCENARIO in LRemoteStartEndProcess LRemoteSequentialFlowProcess LRemoteParallelGatewayProcess LRemoteRuleTaskProcess LRemoteExternalSignalProcess LRemoteHumanTaskProcess
do
	mvn clean install exec:exec $GLOBAL_PARAMS -DreportDataLocation=${REPORT_LOCATION}/threads_${THREADS} -Dsuite=ConcurrentLoadSuite -Dthreads=$THREADS -DwarmUp=true -DwarmUpCount=10 -Dscenario=${SCENARIO}
done

###############################################
#50 Threads
###############################################
THREADS=50

for SCENARIO in LRemoteStartEndProcess LRemoteSequentialFlowProcess LRemoteParallelGatewayProcess LRemoteRuleTaskProcess LRemoteExternalSignalProcess LRemoteHumanTaskProcess
do
	mvn clean install exec:exec $GLOBAL_PARAMS -DreportDataLocation=${REPORT_LOCATION}/threads_${THREADS} -Dsuite=ConcurrentLoadSuite -Dthreads=$THREADS -DwarmUp=true -DwarmUpCount=10 -Dscenario=${SCENARIO}
done

###############################################
#75 Threads
###############################################
THREADS=75

for SCENARIO in LRemoteStartEndProcess LRemoteSequentialFlowProcess LRemoteParallelGatewayProcess LRemoteRuleTaskProcess LRemoteExternalSignalProcess LRemoteHumanTaskProcess
do
	mvn clean install exec:exec $GLOBAL_PARAMS -DreportDataLocation=${REPORT_LOCATION}/threads_${THREADS} -Dsuite=ConcurrentLoadSuite -Dthreads=$THREADS -DwarmUp=true -DwarmUpCount=10  -Dscenario=${SCENARIO}
done

###############################################
#100 Threads
###############################################
THREADS=100

for SCENARIO in LRemoteStartEndProcess LRemoteSequentialFlowProcess LRemoteParallelGatewayProcess LRemoteRuleTaskProcess LRemoteExternalSignalProcess LRemoteHumanTaskProcess
do
	mvn clean install exec:exec $GLOBAL_PARAMS -DreportDataLocation=${REPORT_LOCATION}/threads_${THREADS} -Dsuite=ConcurrentLoadSuite -Dthreads=$THREADS -DwarmUp=true -DwarmUpCount=10  -Dscenario=${SCENARIO}
done

###############################################
#125 Threads
###############################################
THREADS=125

for SCENARIO in LRemoteStartEndProcess LRemoteSequentialFlowProcess LRemoteParallelGatewayProcess LRemoteRuleTaskProcess LRemoteExternalSignalProcess LRemoteHumanTaskProcess
do
	mvn clean install exec:exec $GLOBAL_PARAMS -DreportDataLocation=${REPORT_LOCATION}/threads_${THREADS} -Dsuite=ConcurrentLoadSuite -Dthreads=$THREADS -DwarmUp=true -DwarmUpCount=10 -Dscenario=${SCENARIO}
done
###############################################
#END No Persistence
###############################################
