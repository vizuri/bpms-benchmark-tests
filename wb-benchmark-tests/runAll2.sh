#/bin/sh

#GLOBAL_PARAMS="-Diterations=10  -DreporterType=CSVSingle -Dworkbench.username=bpmsAdmin -Dworkbench.password=P@ssw0rd -Dworkbench.host=172.31.3.11 -Dworkbench.port=8080 -Dworkbench.remotingPort=4447 -Dworkbench.name=business-central"
GLOBAL_PARAMS="-Diterations=1  -DreporterType=CSVSingle -Dworkbench.username=bpmsAdmin -Dworkbench.password=P@ssw0rd -Dworkbench.host=localhost -Dworkbench.port=8888 -Dworkbench.remotingPort=4447 -Dworkbench.name=business-central"



REPORT_BASE=results-medium-remote
rm -fr $REPORT_BASE

###############################################
#No Persistence
###############################################
REPORT_LOCATION=${REPORT_BASE}/no_persistance

###############################################
#1 Thread
###############################################
THREADS=1

#for SCENARIO in LRemoteStartEndProcess LRemoteSequentialFlowProcess LRemoteParallelGatewayProcess LRemoteRuleTaskProcess LRemoteExternalSignalProcess LRemoteHumanTaskProcess
for SCENARIO in LRemoteRuleTaskProcess
do
	mvn clean install exec:exec $GLOBAL_PARAMS -DreportDataLocation=${REPORT_LOCATION}/threads_${THREADS} -Dsuite=ConcurrentLoadSuite -Dthreads=$THREADS -DwarmUp=false -DwarmUpCount=0 -Dscenario=${SCENARIO}
done
