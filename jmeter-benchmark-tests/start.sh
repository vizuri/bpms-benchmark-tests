#!/bin/bash
 
export JMETER_HOME=./apache-jmeter-3.0
export PATH=$PATH:$JMETER_HOME/bin
 
START=$(date +"%Y-%m-%d_%H.%M.%S")
LOG="BPMSBenchmarkTest_${START}.log"
AGGREGATE_CSV="aggregate.${START}.csv"
 
jmeter.sh -t BPMSWorkBenchTests.jmx
 

