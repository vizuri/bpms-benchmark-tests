#!/bin/bash
 
export JMETER_HOME=./apache-jmeter-3.0
export PATH=$PATH:$JMETER_HOME/bin
 
START=$(date +"%Y-%m-%d_%H.%M.%S")
LOG="BPMSBenchmarkTest_${START}.jtl"
AGGREGATE_CSV="aggregate.${START}.csv"
 
jmeter.sh -t BPMSWorkBenchTests.jmx -n -l $LOG
 
java -jar ./apache-jmeter-3.0/lib/ext/CMDRunner.jar --tool Reporter --generate-csv $AGGREGATE_CSV --input-jtl $LOG --plugin-type AggregateReport
